import { Component, ElementRef, ViewChild } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: FirebaseListObservable<any>;
   rooms: FirebaseListObservable<any>;
  name: any;
  msgVal: string = '';
  @ViewChild('message1') private myScrollContainer: ElementRef;

    constructor(public af: AngularFire) {
    this.items = af.database.list('/messages', {
      query: {
        limitToLast: 30
      }
    });


              setTimeout(() => {
            debugger;
      this.scrollToBottom();
    });

    this.af.auth.subscribe(auth => { 
      if(auth) {
        this.name = auth;
      }
    });

  }


chatSend(theirMessage: string) {
      this.items.push({ message: theirMessage, name: this.name.facebook.displayName});
      this.msgVal = '';
  }

    scrollToBottom(): void {
        try {
          debugger;
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }                 
    }


}