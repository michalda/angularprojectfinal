import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-Contact',
  templateUrl: './Contact.component.html',
  styleUrls: ['./Contact.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
})
export class ContactComponent implements OnInit {
 ContactNameVal: string = '';
 Contacts: FirebaseListObservable<any>;
    @ViewChild('created') private mycreatedContainer: ElementRef;
  constructor(public af: AngularFire) {

      this.Contacts = af.database.list('/Contact', {
      query: {
        limitToLast: 30
      }
    });
   }

  ngOnInit() {
  }

  addNewContact(ContactName: string)
  {
    debugger;
     this.Contacts.push({ name: ContactName});
     alert("The Contact '" +ContactName + "' created")
     this.mycreatedContainer.nativeElement.value = null
  }

  

}
