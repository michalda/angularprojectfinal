import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';
import { SignupComponent } from './signup/signup.component';
import { MembersComponent } from './members/members.component';
import { AuthGuard } from './auth.service';
import { routes } from './app.routes';
import { RoomComponent } from './room/room.component';
import { ContactComponent } from './contact/contact.component';


export const firebaseConfig = {
    apiKey: "AIzaSyCwiR4ueLQs4_SVRCpTDv0TXzFHGvqMfz4",
    authDomain: "chatproject-b488b.firebaseapp.com",
    databaseURL: "https://chatproject-b488b.firebaseio.com",
    projectId: "chatproject-b488b",
    storageBucket: "chatproject-b488b.appspot.com",
    messagingSenderId: "1089803835038"
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    LoginComponent,
    MembersComponent,
    RoomComponent,
    ContactComponent
  ],
imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routes
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
