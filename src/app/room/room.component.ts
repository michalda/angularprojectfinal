import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
})
export class RoomComponent implements OnInit {
 roomNameVal: string = '';
  usersVal: string = '';
    myEmail: any;
 rooms: FirebaseListObservable<any>;
    @ViewChild('created') private mycreatedContainer: ElementRef;
    @ViewChild('users') private myusersContainer: ElementRef;
  constructor(public af: AngularFire) {

      this.rooms = af.database.list('/room', {
      query: {
        limitToLast: 30
      }
    });
   }

  ngOnInit() {
  }

  addNewRoom(roomName: string, usersVal: string)
  {
    debugger;
    if (roomName == "")
    {
        alert("Please enter room name, this field cannot be empty");
    }
    else
    {
      var id = Math.floor(Math.random() * (100000 - 1 + 1)) + 1;
          this.af.auth.subscribe(auth => { 
            if(auth) {
              this.myEmail = auth.auth.email;
            }
          });

          var allUsers = usersVal + "," + this.myEmail;
          var key = this.rooms.push({ name: roomName, id: id, users: allUsers, administrator: this.myEmail });
          this.rooms.update(key.key, {key: key.key});
          alert("The room '" + roomName + "' created");
          this.mycreatedContainer.nativeElement.value = null;
          this.myusersContainer.nativeElement.value = null;
    }
    
  }

  

}
