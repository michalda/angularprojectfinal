import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class MembersComponent implements OnInit {
  name: any;
  state: string = '';
   roomId: any;
   roomKey :any;
   messagesByRoomId : any;
   currentUser : any;
   usersOld: any;
   usersToShow: any;
  items: FirebaseListObservable<any>;
     rooms: FirebaseListObservable<any>;
  msgVal: string = '';
  administrator: any;
  aff : any;
   @ViewChild('message1') private myScrollContainer: ElementRef;
   
      @ViewChild('usersToAdd') private myusersToAddContainer: ElementRef;


  constructor(public af: AngularFire,private router: Router) {
        debugger;
        this.aff = af;
          this.rooms = af.database.list('/room', {
      query: {
        limitToLast: 30
      }
    });
    this.af.auth.subscribe(auth => {
      if(auth) {
        debugger;
        this.name = auth;
      }

    });

        debugger;
        this.items = af.database.list('/messages', {
      query: {
        limitToLast: 30
      }
    });


          setTimeout(() => {
            debugger;
      this.scrollToBottom();
    });

        this.af.auth.subscribe(auth => { 
      if(auth) {
        this.name = auth;
      }
    });

  }
  

  logout() {
     this.af.auth.logout();
     console.log('logged out');
     this.router.navigateByUrl('/login');
  }
  room() {
     this.af.auth.logout();
     this.router.navigateByUrl('/members');
  }

  checkAuthenticatedRoomsByUsername(users: any)
  {
    debugger;
    var users = users;
    var currentUser = this.af.auth.subscribe(auth => { 
      if(auth) {
        this.currentUser = auth;
      }
    });
    var currentEmail = String(this.currentUser.auth.email);
    return String(users).search(currentEmail) > -1; 

  }

    addRoom() {
      debugger;
     //this.af.auth.logout();
     this.router.navigateByUrl('/room');
  }
    addContact() {
      debugger;
     //this.af.auth.logout();
     this.router.navigateByUrl('/contact');
  }

  openChatByRoom(roomKey: any, roomId: any, administrator: any, users: any) {
    debugger;
     this.messagesByRoomId = this.items;
     this.roomId = roomId;
     this.roomKey = roomKey;
     this.usersOld = users;
     this.usersToShow = "ROOM FREINDS ARE: "+ users
     this.administrator = administrator;
  }

  addAdditionalUsers(roomKey: any,roomId :any, usersOld:any, usersToAdd: any) {
    debugger;
    if (usersToAdd == undefined || usersToAdd == "")
    {
      alert("Please enter users to add, this field cannot be empty");
    }
    else
    {
      this.rooms.update(roomKey, {users : usersOld + "," + usersToAdd});
      alert("The users '" + usersToAdd + "' Added");
      this.myusersToAddContainer.nativeElement.value = null;
      this.router.navigateByUrl('/login');
    }

  }

removeUsers(roomKey: any,roomId :any, usersOld:any, usersToRemove: any) {
    debugger;
    if (usersToRemove == undefined || usersToRemove == "")
    {
      alert("Please enter users to remove, this field cannot be empty");
    }
    else
    {
      var array = usersToRemove.split(",")
      array.forEach(element => {
        usersOld = usersOld.replace(element + ",", "");
        usersOld = usersOld.replace("," + element, "");
      });
      this.rooms.update(roomKey, {users : usersOld });
      alert("The users '" + usersToRemove + "' Removed");
      this.myusersToAddContainer.nativeElement.value = null;
      this.router.navigateByUrl('/login');  
    }
    
    
  }

  scrollToBottom(): void {
        try {
          debugger;
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }                 
    }

  chatSend(theirMessage: string, userName: string, emailName: string, photoURL:any, roomId: any) {
    debugger;
    if (theirMessage == "")
    {
      alert("Please enter message, this field cannot be empty");
    }
    else
    {
      var userNameToSave = userName != null ? userName : emailName;
      var date = new Date();
      this.items.push({ message: theirMessage, name:userNameToSave, roomId: roomId, time: date.toLocaleDateString() + " " + date.toLocaleTimeString(), photoURL: photoURL});
      this.msgVal = '';
      setTimeout(() => {
      this.scrollToBottom();
    });
    }

      
  }


  ngOnInit() {
    debugger;
    this.scrollToBottom();
  }



}
